#region Import the Assemblies
[reflection.assembly]::loadwithpartialname("System.Windows.Forms") | Out-Null
[reflection.assembly]::loadwithpartialname("System.Drawing") | Out-Null
#endregion

#region Generated Form Objects
$form1 = New-Object System.Windows.Forms.Form
$Text1 = New-Object System.Windows.Forms.RichTextBox
$InitialFormWindowState = New-Object System.Windows.Forms.FormWindowState
#endregion Generated Form Objects

#region Generated Form Code
$form1.Text = "File Comment"
$form1.Name = "form1"
$form1.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Width = 490
$System_Drawing_Size.Height = 219
$form1.ClientSize = $System_Drawing_Size
$Text1.Name = "Text1"
$Text1.Text = ""
$DS = "Fill"
$Text1.Dock = [System.Windows.Forms.DockStyle]::Fill
$Text1.DataBindings.DefaultDataSourceUpdateMode = 0
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 24
$System_Drawing_Point.Y = 36
$Text1.Location = $System_Drawing_Point
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Width = 175
$System_Drawing_Size.Height = 29
$Text1.Size = $System_Drawing_Size
$Text1.TabIndex = 2
$Text1.add_TextChanged($handler_EnterComputerName_TextChanged)

$form1.Controls.Add($Text1)

#endregion Generated Form Code

#Save the initial state of the form
$InitialFormWindowState = $form1.WindowState
#Init the onload event to correct the initial state of the form

Add-Type -Path "c:\Program Files (x86)\Autodesk\Autodesk Vault 2014 SDK\bin\Autodesk.DataManagement.Client.Framework.Vault.Forms.dll"
$global:g_login=[Autodesk.DataManagement.Client.Framework.Vault.Forms.Library]::Login($null)
$docSvc = $g_login.WebServiceManager.DocumentService
# Назначим DocumentService переменной для быстрого доступа
$ses = New-Object Autodesk.DataManagement.Client.Framework.Vault.Forms.Settings.SelectEntitySettings
$ses.ActionableEntityClassID = 'FILE'
$ses.MultipleSelect = $false
$dialogResult = [Autodesk.DataManagement.Client.Framework.Vault.Forms.Library]::SelectEntity($g_login,$ses)
# Предоставим пользователю возможность выбрать файл
$file = $dialogResult.SelectedEntities.Item(0)
$Text1.Text = $file.Comment
$form1.ShowDialog()| Out-Null
#$file.Comment | Out-GridView